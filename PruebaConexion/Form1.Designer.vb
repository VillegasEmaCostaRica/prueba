﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Dim NoCEDULALabel As System.Windows.Forms.Label
        Dim _1__APELLIDOLabel As System.Windows.Forms.Label
        Dim _2__APELLIDOLabel As System.Windows.Forms.Label
        Dim NOMBRELabel As System.Windows.Forms.Label
        Dim NOMBRE_COMPLETOLabel As System.Windows.Forms.Label
        Dim E_MAILLabel As System.Windows.Forms.Label
        Dim CODPUESTOLabel As System.Windows.Forms.Label
        Dim FECHA_INGRESOLabel As System.Windows.Forms.Label
        Dim AÑOSSERVLabel As System.Windows.Forms.Label
        Dim N__CONYUGESLabel As System.Windows.Forms.Label
        Dim N__HIJOSLabel As System.Windows.Forms.Label
        Dim GRADO_ACADEMICOLabel As System.Windows.Forms.Label
        Dim BANCO_A_DEPOSITARLabel As System.Windows.Forms.Label
        Dim NoCUENTALabel As System.Windows.Forms.Label
        Dim TelefonoLabel As System.Windows.Forms.Label
        Dim CelularLabel As System.Windows.Forms.Label
        Dim DireccionLabel As System.Windows.Forms.Label
        Me.Planilla2DataSet = New PruebaConexion.Planilla2DataSet()
        Me.DatosPersonalesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DatosPersonalesTableAdapter = New PruebaConexion.Planilla2DataSetTableAdapters.DatosPersonalesTableAdapter()
        Me.TableAdapterManager = New PruebaConexion.Planilla2DataSetTableAdapters.TableAdapterManager()
        Me.DatosPersonalesBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorAddNewItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.DatosPersonalesBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.NoCEDULATextBox = New System.Windows.Forms.TextBox()
        Me._1__APELLIDOTextBox = New System.Windows.Forms.TextBox()
        Me._2__APELLIDOTextBox = New System.Windows.Forms.TextBox()
        Me.NOMBRETextBox = New System.Windows.Forms.TextBox()
        Me.NOMBRE_COMPLETOTextBox = New System.Windows.Forms.TextBox()
        Me.E_MAILTextBox = New System.Windows.Forms.TextBox()
        Me.CODPUESTOTextBox = New System.Windows.Forms.TextBox()
        Me.FECHA_INGRESODateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.AÑOSSERVTextBox = New System.Windows.Forms.TextBox()
        Me.N__CONYUGESCheckBox = New System.Windows.Forms.CheckBox()
        Me.N__HIJOSTextBox = New System.Windows.Forms.TextBox()
        Me.GRADO_ACADEMICOTextBox = New System.Windows.Forms.TextBox()
        Me.BANCO_A_DEPOSITARTextBox = New System.Windows.Forms.TextBox()
        Me.NoCUENTATextBox = New System.Windows.Forms.TextBox()
        Me.TelefonoTextBox = New System.Windows.Forms.TextBox()
        Me.CelularTextBox = New System.Windows.Forms.TextBox()
        Me.DireccionTextBox = New System.Windows.Forms.TextBox()
        NoCEDULALabel = New System.Windows.Forms.Label()
        _1__APELLIDOLabel = New System.Windows.Forms.Label()
        _2__APELLIDOLabel = New System.Windows.Forms.Label()
        NOMBRELabel = New System.Windows.Forms.Label()
        NOMBRE_COMPLETOLabel = New System.Windows.Forms.Label()
        E_MAILLabel = New System.Windows.Forms.Label()
        CODPUESTOLabel = New System.Windows.Forms.Label()
        FECHA_INGRESOLabel = New System.Windows.Forms.Label()
        AÑOSSERVLabel = New System.Windows.Forms.Label()
        N__CONYUGESLabel = New System.Windows.Forms.Label()
        N__HIJOSLabel = New System.Windows.Forms.Label()
        GRADO_ACADEMICOLabel = New System.Windows.Forms.Label()
        BANCO_A_DEPOSITARLabel = New System.Windows.Forms.Label()
        NoCUENTALabel = New System.Windows.Forms.Label()
        TelefonoLabel = New System.Windows.Forms.Label()
        CelularLabel = New System.Windows.Forms.Label()
        DireccionLabel = New System.Windows.Forms.Label()
        CType(Me.Planilla2DataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DatosPersonalesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DatosPersonalesBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.DatosPersonalesBindingNavigator.SuspendLayout()
        Me.SuspendLayout()
        '
        'Planilla2DataSet
        '
        Me.Planilla2DataSet.DataSetName = "Planilla2DataSet"
        Me.Planilla2DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DatosPersonalesBindingSource
        '
        Me.DatosPersonalesBindingSource.DataMember = "DatosPersonales"
        Me.DatosPersonalesBindingSource.DataSource = Me.Planilla2DataSet
        '
        'DatosPersonalesTableAdapter
        '
        Me.DatosPersonalesTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.AjustesTableAdapter = Nothing
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.DatosPersonalesTableAdapter = Me.DatosPersonalesTableAdapter
        Me.TableAdapterManager.DeduccionesTableAdapter = Nothing
        Me.TableAdapterManager.PlanillaBisemanalTableAdapter = Nothing
        Me.TableAdapterManager.PlanillaMensualTableAdapter = Nothing
        Me.TableAdapterManager.PuestosTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = PruebaConexion.Planilla2DataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'DatosPersonalesBindingNavigator
        '
        Me.DatosPersonalesBindingNavigator.AddNewItem = Me.BindingNavigatorAddNewItem
        Me.DatosPersonalesBindingNavigator.BindingSource = Me.DatosPersonalesBindingSource
        Me.DatosPersonalesBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.DatosPersonalesBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.DatosPersonalesBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2, Me.BindingNavigatorAddNewItem, Me.BindingNavigatorDeleteItem, Me.DatosPersonalesBindingNavigatorSaveItem})
        Me.DatosPersonalesBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.DatosPersonalesBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.DatosPersonalesBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.DatosPersonalesBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.DatosPersonalesBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.DatosPersonalesBindingNavigator.Name = "DatosPersonalesBindingNavigator"
        Me.DatosPersonalesBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.DatosPersonalesBindingNavigator.Size = New System.Drawing.Size(400, 25)
        Me.DatosPersonalesBindingNavigator.TabIndex = 0
        Me.DatosPersonalesBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Move first"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Move previous"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Position"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 23)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Current position"
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(35, 15)
        Me.BindingNavigatorCountItem.Text = "of {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Total number of items"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 6)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 20)
        Me.BindingNavigatorMoveNextItem.Text = "Move next"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 20)
        Me.BindingNavigatorMoveLastItem.Text = "Move last"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 6)
        '
        'BindingNavigatorAddNewItem
        '
        Me.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorAddNewItem.Image = CType(resources.GetObject("BindingNavigatorAddNewItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem"
        Me.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorAddNewItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorAddNewItem.Text = "Add new"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(23, 20)
        Me.BindingNavigatorDeleteItem.Text = "Delete"
        '
        'DatosPersonalesBindingNavigatorSaveItem
        '
        Me.DatosPersonalesBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.DatosPersonalesBindingNavigatorSaveItem.Image = CType(resources.GetObject("DatosPersonalesBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.DatosPersonalesBindingNavigatorSaveItem.Name = "DatosPersonalesBindingNavigatorSaveItem"
        Me.DatosPersonalesBindingNavigatorSaveItem.Size = New System.Drawing.Size(23, 23)
        Me.DatosPersonalesBindingNavigatorSaveItem.Text = "Save Data"
        '
        'NoCEDULALabel
        '
        NoCEDULALabel.AutoSize = True
        NoCEDULALabel.Location = New System.Drawing.Point(52, 34)
        NoCEDULALabel.Name = "NoCEDULALabel"
        NoCEDULALabel.Size = New System.Drawing.Size(70, 13)
        NoCEDULALabel.TabIndex = 1
        NoCEDULALabel.Text = "No CEDULA:"
        '
        'NoCEDULATextBox
        '
        Me.NoCEDULATextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "NoCEDULA", True))
        Me.NoCEDULATextBox.Location = New System.Drawing.Point(180, 31)
        Me.NoCEDULATextBox.Name = "NoCEDULATextBox"
        Me.NoCEDULATextBox.Size = New System.Drawing.Size(200, 20)
        Me.NoCEDULATextBox.TabIndex = 2
        '
        '_1__APELLIDOLabel
        '
        _1__APELLIDOLabel.AutoSize = True
        _1__APELLIDOLabel.Location = New System.Drawing.Point(52, 60)
        _1__APELLIDOLabel.Name = "_1__APELLIDOLabel"
        _1__APELLIDOLabel.Size = New System.Drawing.Size(75, 13)
        _1__APELLIDOLabel.TabIndex = 3
        _1__APELLIDOLabel.Text = "1° APELLIDO:"
        '
        '_1__APELLIDOTextBox
        '
        Me._1__APELLIDOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "1° APELLIDO", True))
        Me._1__APELLIDOTextBox.Location = New System.Drawing.Point(180, 57)
        Me._1__APELLIDOTextBox.Name = "_1__APELLIDOTextBox"
        Me._1__APELLIDOTextBox.Size = New System.Drawing.Size(200, 20)
        Me._1__APELLIDOTextBox.TabIndex = 4
        '
        '_2__APELLIDOLabel
        '
        _2__APELLIDOLabel.AutoSize = True
        _2__APELLIDOLabel.Location = New System.Drawing.Point(52, 86)
        _2__APELLIDOLabel.Name = "_2__APELLIDOLabel"
        _2__APELLIDOLabel.Size = New System.Drawing.Size(75, 13)
        _2__APELLIDOLabel.TabIndex = 5
        _2__APELLIDOLabel.Text = "2° APELLIDO:"
        '
        '_2__APELLIDOTextBox
        '
        Me._2__APELLIDOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "2° APELLIDO", True))
        Me._2__APELLIDOTextBox.Location = New System.Drawing.Point(180, 83)
        Me._2__APELLIDOTextBox.Name = "_2__APELLIDOTextBox"
        Me._2__APELLIDOTextBox.Size = New System.Drawing.Size(200, 20)
        Me._2__APELLIDOTextBox.TabIndex = 6
        '
        'NOMBRELabel
        '
        NOMBRELabel.AutoSize = True
        NOMBRELabel.Location = New System.Drawing.Point(52, 112)
        NOMBRELabel.Name = "NOMBRELabel"
        NOMBRELabel.Size = New System.Drawing.Size(57, 13)
        NOMBRELabel.TabIndex = 7
        NOMBRELabel.Text = "NOMBRE:"
        '
        'NOMBRETextBox
        '
        Me.NOMBRETextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "NOMBRE", True))
        Me.NOMBRETextBox.Location = New System.Drawing.Point(180, 109)
        Me.NOMBRETextBox.Name = "NOMBRETextBox"
        Me.NOMBRETextBox.Size = New System.Drawing.Size(200, 20)
        Me.NOMBRETextBox.TabIndex = 8
        '
        'NOMBRE_COMPLETOLabel
        '
        NOMBRE_COMPLETOLabel.AutoSize = True
        NOMBRE_COMPLETOLabel.Location = New System.Drawing.Point(52, 138)
        NOMBRE_COMPLETOLabel.Name = "NOMBRE_COMPLETOLabel"
        NOMBRE_COMPLETOLabel.Size = New System.Drawing.Size(119, 13)
        NOMBRE_COMPLETOLabel.TabIndex = 9
        NOMBRE_COMPLETOLabel.Text = "NOMBRE COMPLETO:"
        '
        'NOMBRE_COMPLETOTextBox
        '
        Me.NOMBRE_COMPLETOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "NOMBRE COMPLETO", True))
        Me.NOMBRE_COMPLETOTextBox.Location = New System.Drawing.Point(180, 135)
        Me.NOMBRE_COMPLETOTextBox.Name = "NOMBRE_COMPLETOTextBox"
        Me.NOMBRE_COMPLETOTextBox.Size = New System.Drawing.Size(200, 20)
        Me.NOMBRE_COMPLETOTextBox.TabIndex = 10
        '
        'E_MAILLabel
        '
        E_MAILLabel.AutoSize = True
        E_MAILLabel.Location = New System.Drawing.Point(52, 164)
        E_MAILLabel.Name = "E_MAILLabel"
        E_MAILLabel.Size = New System.Drawing.Size(45, 13)
        E_MAILLabel.TabIndex = 11
        E_MAILLabel.Text = "E-MAIL:"
        '
        'E_MAILTextBox
        '
        Me.E_MAILTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "E-MAIL", True))
        Me.E_MAILTextBox.Location = New System.Drawing.Point(180, 161)
        Me.E_MAILTextBox.Name = "E_MAILTextBox"
        Me.E_MAILTextBox.Size = New System.Drawing.Size(200, 20)
        Me.E_MAILTextBox.TabIndex = 12
        '
        'CODPUESTOLabel
        '
        CODPUESTOLabel.AutoSize = True
        CODPUESTOLabel.Location = New System.Drawing.Point(52, 190)
        CODPUESTOLabel.Name = "CODPUESTOLabel"
        CODPUESTOLabel.Size = New System.Drawing.Size(77, 13)
        CODPUESTOLabel.TabIndex = 13
        CODPUESTOLabel.Text = "CODPUESTO:"
        '
        'CODPUESTOTextBox
        '
        Me.CODPUESTOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "CODPUESTO", True))
        Me.CODPUESTOTextBox.Location = New System.Drawing.Point(180, 187)
        Me.CODPUESTOTextBox.Name = "CODPUESTOTextBox"
        Me.CODPUESTOTextBox.Size = New System.Drawing.Size(200, 20)
        Me.CODPUESTOTextBox.TabIndex = 14
        '
        'FECHA_INGRESOLabel
        '
        FECHA_INGRESOLabel.AutoSize = True
        FECHA_INGRESOLabel.Location = New System.Drawing.Point(52, 217)
        FECHA_INGRESOLabel.Name = "FECHA_INGRESOLabel"
        FECHA_INGRESOLabel.Size = New System.Drawing.Size(97, 13)
        FECHA_INGRESOLabel.TabIndex = 15
        FECHA_INGRESOLabel.Text = "FECHA INGRESO:"
        '
        'FECHA_INGRESODateTimePicker
        '
        Me.FECHA_INGRESODateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.DatosPersonalesBindingSource, "FECHA INGRESO", True))
        Me.FECHA_INGRESODateTimePicker.Location = New System.Drawing.Point(180, 213)
        Me.FECHA_INGRESODateTimePicker.Name = "FECHA_INGRESODateTimePicker"
        Me.FECHA_INGRESODateTimePicker.Size = New System.Drawing.Size(200, 20)
        Me.FECHA_INGRESODateTimePicker.TabIndex = 16
        '
        'AÑOSSERVLabel
        '
        AÑOSSERVLabel.AutoSize = True
        AÑOSSERVLabel.Location = New System.Drawing.Point(52, 242)
        AÑOSSERVLabel.Name = "AÑOSSERVLabel"
        AÑOSSERVLabel.Size = New System.Drawing.Size(69, 13)
        AÑOSSERVLabel.TabIndex = 17
        AÑOSSERVLabel.Text = "AÑOSSERV:"
        '
        'AÑOSSERVTextBox
        '
        Me.AÑOSSERVTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "AÑOSSERV", True))
        Me.AÑOSSERVTextBox.Location = New System.Drawing.Point(180, 239)
        Me.AÑOSSERVTextBox.Name = "AÑOSSERVTextBox"
        Me.AÑOSSERVTextBox.Size = New System.Drawing.Size(200, 20)
        Me.AÑOSSERVTextBox.TabIndex = 18
        '
        'N__CONYUGESLabel
        '
        N__CONYUGESLabel.AutoSize = True
        N__CONYUGESLabel.Location = New System.Drawing.Point(52, 270)
        N__CONYUGESLabel.Name = "N__CONYUGESLabel"
        N__CONYUGESLabel.Size = New System.Drawing.Size(85, 13)
        N__CONYUGESLabel.TabIndex = 19
        N__CONYUGESLabel.Text = "N° CONYUGES:"
        '
        'N__CONYUGESCheckBox
        '
        Me.N__CONYUGESCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.DatosPersonalesBindingSource, "N° CONYUGES", True))
        Me.N__CONYUGESCheckBox.Location = New System.Drawing.Point(180, 265)
        Me.N__CONYUGESCheckBox.Name = "N__CONYUGESCheckBox"
        Me.N__CONYUGESCheckBox.Size = New System.Drawing.Size(200, 24)
        Me.N__CONYUGESCheckBox.TabIndex = 20
        Me.N__CONYUGESCheckBox.Text = "CheckBox1"
        Me.N__CONYUGESCheckBox.UseVisualStyleBackColor = True
        '
        'N__HIJOSLabel
        '
        N__HIJOSLabel.AutoSize = True
        N__HIJOSLabel.Location = New System.Drawing.Point(52, 298)
        N__HIJOSLabel.Name = "N__HIJOSLabel"
        N__HIJOSLabel.Size = New System.Drawing.Size(56, 13)
        N__HIJOSLabel.TabIndex = 21
        N__HIJOSLabel.Text = "N° HIJOS:"
        '
        'N__HIJOSTextBox
        '
        Me.N__HIJOSTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "N° HIJOS", True))
        Me.N__HIJOSTextBox.Location = New System.Drawing.Point(180, 295)
        Me.N__HIJOSTextBox.Name = "N__HIJOSTextBox"
        Me.N__HIJOSTextBox.Size = New System.Drawing.Size(200, 20)
        Me.N__HIJOSTextBox.TabIndex = 22
        '
        'GRADO_ACADEMICOLabel
        '
        GRADO_ACADEMICOLabel.AutoSize = True
        GRADO_ACADEMICOLabel.Location = New System.Drawing.Point(52, 324)
        GRADO_ACADEMICOLabel.Name = "GRADO_ACADEMICOLabel"
        GRADO_ACADEMICOLabel.Size = New System.Drawing.Size(115, 13)
        GRADO_ACADEMICOLabel.TabIndex = 23
        GRADO_ACADEMICOLabel.Text = "GRADO ACADEMICO:"
        '
        'GRADO_ACADEMICOTextBox
        '
        Me.GRADO_ACADEMICOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "GRADO ACADEMICO", True))
        Me.GRADO_ACADEMICOTextBox.Location = New System.Drawing.Point(180, 321)
        Me.GRADO_ACADEMICOTextBox.Name = "GRADO_ACADEMICOTextBox"
        Me.GRADO_ACADEMICOTextBox.Size = New System.Drawing.Size(200, 20)
        Me.GRADO_ACADEMICOTextBox.TabIndex = 24
        '
        'BANCO_A_DEPOSITARLabel
        '
        BANCO_A_DEPOSITARLabel.AutoSize = True
        BANCO_A_DEPOSITARLabel.Location = New System.Drawing.Point(52, 350)
        BANCO_A_DEPOSITARLabel.Name = "BANCO_A_DEPOSITARLabel"
        BANCO_A_DEPOSITARLabel.Size = New System.Drawing.Size(122, 13)
        BANCO_A_DEPOSITARLabel.TabIndex = 25
        BANCO_A_DEPOSITARLabel.Text = "BANCO A DEPOSITAR:"
        '
        'BANCO_A_DEPOSITARTextBox
        '
        Me.BANCO_A_DEPOSITARTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "BANCO A DEPOSITAR", True))
        Me.BANCO_A_DEPOSITARTextBox.Location = New System.Drawing.Point(180, 347)
        Me.BANCO_A_DEPOSITARTextBox.Name = "BANCO_A_DEPOSITARTextBox"
        Me.BANCO_A_DEPOSITARTextBox.Size = New System.Drawing.Size(200, 20)
        Me.BANCO_A_DEPOSITARTextBox.TabIndex = 26
        '
        'NoCUENTALabel
        '
        NoCUENTALabel.AutoSize = True
        NoCUENTALabel.Location = New System.Drawing.Point(52, 376)
        NoCUENTALabel.Name = "NoCUENTALabel"
        NoCUENTALabel.Size = New System.Drawing.Size(71, 13)
        NoCUENTALabel.TabIndex = 27
        NoCUENTALabel.Text = "No CUENTA:"
        '
        'NoCUENTATextBox
        '
        Me.NoCUENTATextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "NoCUENTA", True))
        Me.NoCUENTATextBox.Location = New System.Drawing.Point(180, 373)
        Me.NoCUENTATextBox.Name = "NoCUENTATextBox"
        Me.NoCUENTATextBox.Size = New System.Drawing.Size(200, 20)
        Me.NoCUENTATextBox.TabIndex = 28
        '
        'TelefonoLabel
        '
        TelefonoLabel.AutoSize = True
        TelefonoLabel.Location = New System.Drawing.Point(52, 402)
        TelefonoLabel.Name = "TelefonoLabel"
        TelefonoLabel.Size = New System.Drawing.Size(52, 13)
        TelefonoLabel.TabIndex = 29
        TelefonoLabel.Text = "Telefono:"
        '
        'TelefonoTextBox
        '
        Me.TelefonoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "Telefono", True))
        Me.TelefonoTextBox.Location = New System.Drawing.Point(180, 399)
        Me.TelefonoTextBox.Name = "TelefonoTextBox"
        Me.TelefonoTextBox.Size = New System.Drawing.Size(200, 20)
        Me.TelefonoTextBox.TabIndex = 30
        '
        'CelularLabel
        '
        CelularLabel.AutoSize = True
        CelularLabel.Location = New System.Drawing.Point(52, 428)
        CelularLabel.Name = "CelularLabel"
        CelularLabel.Size = New System.Drawing.Size(42, 13)
        CelularLabel.TabIndex = 31
        CelularLabel.Text = "Celular:"
        '
        'CelularTextBox
        '
        Me.CelularTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "Celular", True))
        Me.CelularTextBox.Location = New System.Drawing.Point(180, 425)
        Me.CelularTextBox.Name = "CelularTextBox"
        Me.CelularTextBox.Size = New System.Drawing.Size(200, 20)
        Me.CelularTextBox.TabIndex = 32
        '
        'DireccionLabel
        '
        DireccionLabel.AutoSize = True
        DireccionLabel.Location = New System.Drawing.Point(52, 454)
        DireccionLabel.Name = "DireccionLabel"
        DireccionLabel.Size = New System.Drawing.Size(55, 13)
        DireccionLabel.TabIndex = 33
        DireccionLabel.Text = "Direccion:"
        '
        'DireccionTextBox
        '
        Me.DireccionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DatosPersonalesBindingSource, "Direccion", True))
        Me.DireccionTextBox.Location = New System.Drawing.Point(180, 451)
        Me.DireccionTextBox.Name = "DireccionTextBox"
        Me.DireccionTextBox.Size = New System.Drawing.Size(200, 20)
        Me.DireccionTextBox.TabIndex = 34
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(400, 491)
        Me.Controls.Add(NoCEDULALabel)
        Me.Controls.Add(Me.NoCEDULATextBox)
        Me.Controls.Add(_1__APELLIDOLabel)
        Me.Controls.Add(Me._1__APELLIDOTextBox)
        Me.Controls.Add(_2__APELLIDOLabel)
        Me.Controls.Add(Me._2__APELLIDOTextBox)
        Me.Controls.Add(NOMBRELabel)
        Me.Controls.Add(Me.NOMBRETextBox)
        Me.Controls.Add(NOMBRE_COMPLETOLabel)
        Me.Controls.Add(Me.NOMBRE_COMPLETOTextBox)
        Me.Controls.Add(E_MAILLabel)
        Me.Controls.Add(Me.E_MAILTextBox)
        Me.Controls.Add(CODPUESTOLabel)
        Me.Controls.Add(Me.CODPUESTOTextBox)
        Me.Controls.Add(FECHA_INGRESOLabel)
        Me.Controls.Add(Me.FECHA_INGRESODateTimePicker)
        Me.Controls.Add(AÑOSSERVLabel)
        Me.Controls.Add(Me.AÑOSSERVTextBox)
        Me.Controls.Add(N__CONYUGESLabel)
        Me.Controls.Add(Me.N__CONYUGESCheckBox)
        Me.Controls.Add(N__HIJOSLabel)
        Me.Controls.Add(Me.N__HIJOSTextBox)
        Me.Controls.Add(GRADO_ACADEMICOLabel)
        Me.Controls.Add(Me.GRADO_ACADEMICOTextBox)
        Me.Controls.Add(BANCO_A_DEPOSITARLabel)
        Me.Controls.Add(Me.BANCO_A_DEPOSITARTextBox)
        Me.Controls.Add(NoCUENTALabel)
        Me.Controls.Add(Me.NoCUENTATextBox)
        Me.Controls.Add(TelefonoLabel)
        Me.Controls.Add(Me.TelefonoTextBox)
        Me.Controls.Add(CelularLabel)
        Me.Controls.Add(Me.CelularTextBox)
        Me.Controls.Add(DireccionLabel)
        Me.Controls.Add(Me.DireccionTextBox)
        Me.Controls.Add(Me.DatosPersonalesBindingNavigator)
        Me.Name = "Form1"
        Me.Text = "Form1"
        CType(Me.Planilla2DataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DatosPersonalesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DatosPersonalesBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.DatosPersonalesBindingNavigator.ResumeLayout(False)
        Me.DatosPersonalesBindingNavigator.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Planilla2DataSet As PruebaConexion.Planilla2DataSet
    Friend WithEvents DatosPersonalesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DatosPersonalesTableAdapter As PruebaConexion.Planilla2DataSetTableAdapters.DatosPersonalesTableAdapter
    Friend WithEvents TableAdapterManager As PruebaConexion.Planilla2DataSetTableAdapters.TableAdapterManager
    Friend WithEvents DatosPersonalesBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorAddNewItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorCountItem As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveFirstItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents DatosPersonalesBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents NoCEDULATextBox As System.Windows.Forms.TextBox
    Friend WithEvents _1__APELLIDOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents _2__APELLIDOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NOMBRETextBox As System.Windows.Forms.TextBox
    Friend WithEvents NOMBRE_COMPLETOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents E_MAILTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CODPUESTOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents FECHA_INGRESODateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents AÑOSSERVTextBox As System.Windows.Forms.TextBox
    Friend WithEvents N__CONYUGESCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents N__HIJOSTextBox As System.Windows.Forms.TextBox
    Friend WithEvents GRADO_ACADEMICOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents BANCO_A_DEPOSITARTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NoCUENTATextBox As System.Windows.Forms.TextBox
    Friend WithEvents TelefonoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CelularTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DireccionTextBox As System.Windows.Forms.TextBox

End Class
